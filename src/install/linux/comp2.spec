Summary: comp2
Name: comp2
Version: %VERSION%
Release:  %RELEASE%
Group:Applications
License: Commercial
BuildRoot: /tmp/%USER%/build-root
Prefix: /opt/10x
#AutoReqProv: no
#requires: Xcellerator, RapTor


%description
comp2 submodule


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/opt/10x/comp2
mkdir -p $RPM_BUILD_ROOT/opt/10x/comp2/bin
mkdir -p $RPM_BUILD_ROOT/opt/10x/comp2/config

cp %PROJECT_BASE/src/files/file1 $RPM_BUILD_ROOT/opt/10x/comp2/bin
cp %PROJECT_BASE/src/files/file2 $RPM_BUILD_ROOT/opt/10x/comp2/bin


%files
%defattr(755,root,root,755)
/opt/10x/comp2/bin
